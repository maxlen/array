<?php

$result = [];

$marker = 10;

$sourceData = [3, 4, 5, -2, 10, 11, 12, -1, 0, 7, 8];

foreach ($sourceData as $item1) {
    foreach ($sourceData as $item2) {
        if (in_array([$item1, $item2], $result) || in_array([$item2, $item1], $result)) {
            continue;
        }
        if (($item1 + $item2 == $marker)) {
            $result[] = [$item1, $item2];
        }
    }
}

var_dump($result);