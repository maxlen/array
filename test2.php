<?php

$result = [];

$marker = 0;
$setValue = 0;

$matrix = [
    [1, 0, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
];

$updCoord = [];

foreach ($matrix as $rowNum => $row) {
    foreach ($row as $colNum => $value) {
        if ($value == $marker) {
            $updCoord[$rowNum] = $colNum;
        }
    }
}

foreach ($matrix as $rowNum => $row) {
    foreach ($row as $colNum => $value) {
        if (isset($updCoord[$rowNum]) && key_exists($rowNum, $updCoord)) {
            $result[$rowNum][$colNum] = $setValue;
            continue;
        }

        if (in_array($colNum, array_values($updCoord))) {
            $result[$rowNum][$colNum] = $setValue;
        } else {
            $result[$rowNum][$colNum] = $value;
        }
    }
}

var_dump($result);